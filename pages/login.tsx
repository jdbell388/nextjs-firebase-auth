import React, { useState } from "react";
import { signin } from "../firebase/auth";

export default function Login() {
  const [userData, setUserData] = useState({ email: "", password: "", error: "" });

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const { email, password } = userData;
    try {
      const signedIn = await signin(email, password);
      return signedIn;
    } catch (error) {
      console.log(error);
      if (error.code === "auth/invalid-email") {
        alert("You have entered an invalid Email address");
      }
      if (error.code === "auth/wrong-password") {
        alert("Incorrect email or password. Please try again.");
      }
    }
  };

  const handleChange = (change: any) => {
    const { id } = change.target;
    const { value } = change.target;
    setUserData({ ...userData, [id]: value });
  };

  return (
    <form onSubmit={e => handleSubmit(e)} className="login-form">
      <div className="input-field">
        <input
          type="email"
          id="email"
          onChange={handleChange}
          placeholder="Email"
          value={userData.email}
        />
      </div>
      <div className="input-field">
        <input
          type="password"
          id="password"
          onChange={handleChange}
          placeholder="Password"
          value={userData.password}
        />
      </div>
      <div className="input-field bottom30">
        <button className="login" type="submit">
          Login
        </button>
      </div>
    </form>
  );
}
