import React from "react";
import { signOut, withAuthSync } from "../firebase/auth";

function profile() {
  return (
    <div>
      <h1>Profile</h1>
      <button onClick={signOut} type="submit">
        Log Out
      </button>
    </div>
  );
}

export default withAuthSync(profile);
